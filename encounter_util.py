import dice_roller
import pandas as pd


# Dessarin Valley Encounters
def dessarin_valley(party_level: str, campaign_setting: str) -> str:
    """Generates random encounter in Dessarin Valley

    Args:
        party_level (str): Input options: 'low', 'mid', 'high'
        campaign_setting (str): Input options: 'day', 'night', 'river'

    Returns:
        encounter (str): Encounter description.
    """
    d20 = dice_roller.multiple_rolls_w_mods(1, 20)[0]
    if d20 < 18:
        encounter = 'No encounter'
    else:
        d12 = dice_roller.multiple_rolls_w_mods(1, 12)[0]
        d8 = dice_roller.multiple_rolls_w_mods(1, 8)[0]
        encounter_roll = d12 + d8

        # RIVER
        if campaign_setting == 'river':            
            # Aarakocra scouts
            if (encounter_roll == 2) or (encounter_roll == 3):
                num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                encounter = f'These {num} Aarakocra scouts attack those who appear to be elemental cultists. ' \
                            'Otherwise, the aarakocra might be helpful.'
            # Air cult skyriders
            elif (encounter_roll == 4) or (encounter_roll == 5):
                num = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                encounter = f'One Feathergale knight leads {num} skyweavers and they are all riding giant vultures.'
            # River Pirates
            elif (encounter_roll == 6) or (encounter_roll == 7) or (encounter_roll == 8) or (encounter_roll == 9):
                num1 = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                num2 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                encounter = f'A keelboat carries {num1} bandit(s), {num2} thug(s), ' \
                            'and has a pirate captain (a bandit captain).'
            # Keelboat
            elif (encounter_roll == 10) or (encounter_roll == 11) or \
                 (encounter_roll == 12) or (encounter_roll == 13) or (encounter_roll == 14):
                num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 4
                num2 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                encounter = f'A river trader\'s keelboat carries {num1} commoner(s) (the sailors), ' \
                            f'{num2} guard(s), and a captain (a spy). They are willing to offer passage' \
                            ' to adventurers heading in the same direction.'
            # Merrow
            elif (encounter_roll == 15) or (encounter_roll == 16):
                num = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                encounter = f'{num} merrow'
            # Ghouls
            elif (encounter_roll == 17) or (encounter_roll == 18):
                num = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                encounter = f'{num} ghouls'
            # Water Elemental
            else:
                encounter = '1 water elemental'
        # NOT-RIVER
        else:
            # LOW LEVEL PARTY
            if party_level == 'low':
                # Low-Level Party During the Day
                if campaign_setting == 'day':
                    # Aarakocra scouts
                    if encounter_roll == 2:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'These {num} Aarakocra scouts attack those who appear to be elemental cultists. ' \
                                    'Otherwise, the aarakocra might be helpful.'
                    # Knights of Samular
                    elif encounter_roll == 3:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = f'This armed patrol consists of {num1} veteran(s) and {num2} guard(s). They hail ' \
                                    'from Summit Hall and offer a hearty  "Well Met!" to the characters'
                    # Pilgrims
                    elif encounter_roll == 4:
                        num1 = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        num3 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = f'A group of pilgrims includes {num1} commoner(s), {num2} guard(s), ' \
                                    f'{num3} acolyte(s), and a priest bound for a holy site. They\'re happy ' \
                                    'for company.'
                    # Elk tribe hunters
                    elif encounter_roll == 5:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'This group includes a berserker and {num} tribal warriors. They are hostile.'
                    # Ankhegs
                    elif encounter_roll == 6:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} ankheg(s)'
                    # Bugbears
                    elif encounter_roll == 7:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0] + 1
                        encounter = f'{num} bugbear(s)'
                    # Orcs
                    elif encounter_roll == 8:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'{num} orc(s)'
                    # Dwarf miners
                    elif encounter_roll == 9:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'A band of dwarf miners consists of {num} shield dwarf scouts and a ' \
                                    'pugnacious leader (a shield dwarf thug).'
                    # Caravan
                    elif encounter_roll == 10:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 2
                        num2 = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = 'A caravan consists of a merchant and his or her entourage heading for the ' \
                                    f'nearest settlement. The group consists of {num1} guard(s), ' \
                                    f'{num2} commoner(s), and the caravan lead (a spy).'
                    # Homestead
                    elif encounter_roll == 11:
                        d6 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        if (d6 == 1) or (d6 == 2) or (d6 == 3):
                            type_ppl = 'Tethyrian human'
                        elif d6 == 4:
                            type_ppl = 'Illuskan human'
                        else:
                            type_ppl = 'halfling'
                        num1 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 6)[0] - 1
                        encounter = f'The homestead consists of {num1} {type_ppl}(s) and their {num2} ' \
                                    'children. Residents might provide friendly adventurers with food and shelter.'
                    # Air cult scouts
                    elif encounter_roll == 12:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'A group of scouts for the air cult consists of {num} hurricane(s) in wingwear.'
                    # Water cult marauders
                    elif encounter_roll == 13:
                        num1 = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        num2 = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'A marauder gang of the water cult consists of {num1} ' \
                                    f'Crushing Wave reaver(s), a Crushing Wave priest, and {num2} fathomer(s).'
                    # Earth cult robbers
                    elif encounter_roll == 14:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = 'These earth cult robbers stake out spots to waylay passerby. ' \
                                    f'The group consists f {num1} bandit(s) and {num2} Black Earth guard(s).'
                    # Fire cult raiders
                    elif encounter_roll == 15:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        encounter = f'The fire cult sends out raiders that include {num} ' \
                                    'Eternal Flame guardian(s) and an Eternal Flame priest.'
                    # Gnolls
                    elif encounter_roll == 16:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'{num} gnoll(s)'
                    # Shepherds
                    elif encounter_roll == 17:
                        d6 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        if (d6 == 1) or (d6 == 2) or (d6 == 3) or (d6 == 4):
                            race_of_ppl = 'human'
                        else:
                            race_of_ppl = 'halfling'
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'A group of {race_of_ppl} shepherds watch over herd animals. ' \
                                    f'There are {num1} commoner(s) and {num2} leader(s) (scouts)'
                    # Wolves
                    elif encounter_roll == 18:
                        num = dice_roller.multiple_rolls_w_mods(1, 6)[0] + 2
                        if num > 1:
                            encounter = f'{num} wolves'
                        else:
                            encounter = 'One wolf'
                    # Ogres
                    elif encounter_roll == 19:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} ogre(s)'
                    # Perytons
                    else:
                        num = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'{num} peryton(s)'

                # Low-Level Party at Night
                else:
                    # Jacklewares
                    if encounter_roll == 2:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'{num} jacklewere(s)'
                    # Pilgrims
                    elif encounter_roll == 3:
                        num1 = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        num3 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = f'A group of pilgrims includes {num1} commoner(s), {num2} guard(s), {num3} ' \
                                    'acolyte(s), and a priest bound for a holy site. They\'re happy for company.'
                    # Owlbears
                    elif encounter_roll == 4:
                        num = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'{num} owlbear(s)'
                    # Elk Tribe hunters
                    elif encounter_roll == 5:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'This group includes a berserker and {num} tribal warriors. They are hostile.'
                    # Ankhegs
                    elif encounter_roll == 6:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} ankheg(s)'
                    # Bugbears
                    elif encounter_roll == 7:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0] + 1
                        encounter = f'{num} bugbear(s)'
                    # Orcs
                    elif encounter_roll == 8:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'{num} orc(s)'
                    # Air cult scouts
                    elif encounter_roll == 9:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'A group of scouts for the air cult consists of {num} hurricane(s) in wingwear.'
                    # Water cult marauders
                    elif encounter_roll == 10:
                        num1 = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        num2 = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'A marauder gang of the water cult consists of {num1} ' \
                                    f'Crushing Wave reaver(s), a Crushing Wave priest, and {num2} fathomer(s).'
                    # Earth cult robbers
                    elif encounter_roll == 11:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = 'These earth cult robbers stake out spots to waylay passerby. The group consists '
                        encounter += f'of {num1} bandit(s) and {num2} Black Earth guard(s).'
                    # Fire cult raiders
                    elif encounter_roll == 12:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        encounter = f'The fire cult sends out raiders that include {num} ' \
                                    'Eternal Flame guardian(s) and an Eternal Flame priest.'
                    # Gnolls
                    elif encounter_roll == 13:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'{num} gnoll(s)'
                    # Wolves
                    elif encounter_roll == 14:
                        num = dice_roller.multiple_rolls_w_mods(1, 6)[0] + 2
                        if num > 1:
                            encounter = f'{num} wolves'
                        else:
                            encounter = 'One wolf'
                    # Ogres
                    elif encounter_roll == 15:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} ogre(s)'
                    # Gargoyles
                    elif encounter_roll == 16:
                        num = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'{num} gargoyle(s)'
                    # Ghouls
                    elif encounter_roll == 17:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0] + 1
                        encounter = f'{num} ghoul(s)'
                    # Perytons
                    elif encounter_roll == 18:
                        num = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'{num} peryton(s)'
                    # Wights
                    elif encounter_roll == 19:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} wight(s)'
                    # The Watchful Knight
                    else:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = 'Once, this helmed horror stood watch in the common room of the Inn of the ' \
                                    'Watchful Knight in Beliard. It chooses one character at random, advances to' \
                                    ' within 5 feet then studies the target for several seconds. If attacked, ' \
                                    'it fight backs, retreating after it loses half its hit points. Otherwise, ' \
                                    f'it follows the chosen character for {num} day(s), guarding its temporary master' \
                                    'in combat. At the end of that time the helmed horror wanders off again.'

            # MID LEVEL PARTY
            elif party_level == 'mid':
                # Mid-Level Party During the Day
                if campaign_setting == 'day':
                    # Aarakocra war band
                    if encounter_roll == 2:
                        num = dice_roller.multiple_rolls_w_mods(1, 6)[0] + 3
                        encounter = f'A war band consists of {num} aarakocra and an air elemental. They attack ' \
                                    'those who appear to be elemental cultists. Otherwise, the aarakocra might ' \
                                    'be helpful.'
                    # Manticores
                    elif encounter_roll == 3:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} manticore(s)'
                    # Trolls
                    elif encounter_roll == 4:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0] + 1
                        encounter = f'{num} trolls'
                    # Elk Tribe Hunters
                    elif encounter_roll == 5:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'This group includes a berserker and {num} tribal warriors. They are hostile.'
                    # Knights of Samular
                    elif encounter_roll == 6:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = f'This armed patrol consists of {num1} veteran(s) and {num2} guard(s). They hail ' \
                                    'from Summit Hall and offer a hearty  "Well Met!" to the characters'
                    # Homestead
                    elif encounter_roll == 7:
                        d6 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        if (d6 == 1) or (d6 == 2) or (d6 == 3):
                            type_ppl = 'Tethyrian human'
                        elif d6 == 4:
                            type_ppl = 'Illuskan human'
                        else:
                            type_ppl = 'halfling'
                        num1 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 6)[0] - 1
                        encounter = f'The homestead consists of {num1} {type_ppl}(s) and their {num2} ' \
                                    'children. Residents might provide friendly adventurers with food and shelter.'
                    # Gargoyles
                    elif encounter_roll == 8:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'{num} gargoyles'
                    # Air cult skyriders
                    elif encounter_roll == 9:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = f'One Feathergale knight leads {num} skyweavers and they are riding giant vultures.'
                    # Water cult raiders
                    elif encounter_roll == 10:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        encounter = f'A group of raiders from the water cult includes {num} Crushing Wave reavers, ' \
                                    'a Crushing Wave priest, and a one-eyed shiver. The leader is a Dark Tide Knight ' \
                                    'mounted on a giant crocodile.'
                    # Bugbears
                    elif encounter_roll == 11:
                        num = dice_roller.multiple_rolls_w_mods(1, 6)[0] + 2
                        encounter = f'{num} bugbears'
                    # Fire cult war band
                    elif encounter_roll == 12:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'A war band of the fire cult consists of {num1} Eternal Flame guardians, an ' \
                                    f'Eternal Flame priest, and {num2} hell hounds.'
                    # Earth cult marauders
                    elif encounter_roll == 13:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4) - 1
                        if num2 == 0:
                            encounter = f'A band of marauders for the earth cult consists of {num1} Black Earth ' \
                                        'guards and a Black Earth priest.'
                        else:
                            encounter = f'A band of marauders for the earth cult consists of {num1} Black Earth ' \
                                        f'guards, a Black Earth priest, and {num2} ogre(s).'
                    # Ogres
                    elif encounter_roll == 14:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = f'{num} ogres'
                    # Caravan
                    elif encounter_roll == 15:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 2
                        num2 = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = 'A caravan consists of a merchant and his or her entourage heading for the ' \
                                    f'nearest settlement. The group consists of {num1} guard(s), ' \
                                    f'{num2} commoner(s), and the caravan lead (a spy).'
                    # Mephits
                    elif encounter_roll == 16:
                        d6 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        if d6 == 1:
                            mephit = 'dust'
                        elif d6 == 2:
                            mephit = 'ice'
                        elif d6 == 3:
                            mephit = 'magma'
                        elif d6 == 4:
                            mephit = 'mud'
                        elif d6 == 5:
                            mephit = 'smoke'
                        else:
                            mephit = 'steam'
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = f'{num} {mephit} mephits'
                    # Dwarf miners
                    elif encounter_roll == 17:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'A band of dwarf miners consists of {num} shield dwarf scouts and a ' \
                                    'pugnacious leader (a shield dwarf thug).'
                    # Elementals
                    elif encounter_roll == 18:
                        d4 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        if d4 == 1:
                            elemental = 'air'
                        elif d4 == 2:
                            elemental = 'earth'
                        elif d4 == 3:
                            elemental = 'fire'
                        else:
                            elemental = 'water'
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} {elemental} elementals'
                    # Bulette
                    elif encounter_roll == 19:
                        encounter = 'One bulette'
                    # Hill giants
                    else:
                        num = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'{num} hill giant(s)'

                # Mid-Level Party at Night
                else:
                    # Jackalweres
                    if encounter_roll == 2:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        encounter = f'{num} jackalweres'
                    # Manticores
                    elif encounter_roll == 3:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 3))
                        encounter = f'{num} manticore(s)'
                    # Trolls
                    elif encounter_roll == 4:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 3)) + 1
                        encounter = f'{num} trolls'
                    # Elk Tribe Hunters
                    elif encounter_roll == 5:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'This group includes a berserker and {num} tribal warriors. They are hostile.'
                    # Will-o-wisps
                    elif encounter_roll == 6:
                        num = dice_roller.multiple_rolls_w_mods(1, 8)[0]
                        encounter = f'{num} will-o-wisp(s)'
                    # Ghasts and Ghouls
                    elif encounter_roll == 7:
                        num1 = sum(dice_roller.multiple_rolls_w_mods(1, 2))
                        num2 = sum(dice_roller.multiple_rolls_w_mods(1, 4)) + 2
                        encounter = f'{num1} ghast(s) and {num2} ghouls'
                    # Gargoyles
                    elif encounter_roll == 8:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 4)) + 1
                        encounter = f'{num} gargoyles'
                    # Air cult skyriders
                    elif encounter_roll == 9:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = f'One Feathergale knight leads {num} skyweavers and they are riding giant vultures.'
                    # Water cult raiders
                    elif encounter_roll == 10:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        encounter = f'A group of raiders from the water cult includes {num} Crushing Wave reavers, ' \
                                    'a Crushing Wave priest, and a one-eyed shiver. The leader is a Dark Tide Knight ' \
                                    'mounted on a giant crocodile.'
                    # Bugbears
                    elif encounter_roll == 11:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 6)) + 2
                        encounter = f'{num} bugbears'
                    # Fire cult war band
                    elif encounter_roll == 12:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'A war band of the fire cult consists of {num1} Eternal Flame guardians, an ' \
                                    f'Eternal Flame priest, and {num2} hell hounds.'
                    # Earth cult marauders
                    elif encounter_roll == 13:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4) - 1
                        if num2 == 0:
                            encounter = f'A band of marauders for the earth cult consists of {num1} Black Earth ' \
                                        'guards and a Black Earth priest.'
                        else:
                            encounter = f'A band of marauders for the earth cult consists of {num1} Black Earth ' \
                                        f'guards, a Black Earth priest, and {num2} ogre(s).'
                    # Ogres
                    elif encounter_roll == 14:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = f'{num} ogres'
                    # Wights
                    elif encounter_roll == 15:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 4)) + 1
                        encounter = f'{num} wights'
                    # Mephits
                    elif encounter_roll == 16:
                        d6 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        if d6 == 1:
                            mephit = 'dust'
                        elif d6 == 2:
                            mephit = 'ice'
                        elif d6 == 3:
                            mephit = 'magma'
                        elif d6 == 4:
                            mephit = 'mud'
                        elif d6 == 5:
                            mephit = 'smoke'
                        else:
                            mephit = 'steam'
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = f'{num} {mephit} mephits'
                    # Vampire spawn
                    elif encounter_roll == 17:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 3))
                        encounter = f'{num} vampire spawn'
                    # Elementals
                    elif encounter_roll == 18:
                        d4 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        if d4 == 1:
                            elemental = 'air'
                        elif d4 == 2:
                            elemental = 'earth'
                        elif d4 == 3:
                            elemental = 'fire'
                        else:
                            elemental = 'water'
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} {elemental} elementals'
                    # Bulette
                    elif encounter_roll == 19:
                        encounter = 'One bulette'
                    # Hill giants
                    else:
                        num = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'{num} hill giant(s)'

            # HIGH LEVEL PARTY
            else:
                # High-Level Party during the Day
                if campaign_setting == 'day':
                    # Aarakocra war band
                    if encounter_roll == 2:
                        num = dice_roller.multiple_rolls_w_mods(1, 6)[0] + 3
                        encounter = f'A war band consists of {num} aarakocra and an air elemental. They attack ' \
                                    'those who appear to be elemental cultists. Otherwise, the aarakocra might ' \
                                    'be helpful.'
                    # Manticores
                    elif encounter_roll == 3:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} manticore(s)'
                    # Trolls
                    elif encounter_roll == 4:
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0] + 1
                        encounter = f'{num} trolls'
                    # Elk Tribe Hunters
                    elif encounter_roll == 5:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'This group includes a berserker and {num} tribal warriors. They are hostile.'
                    # Knights of Samular
                    elif encounter_roll == 6:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = f'This armed patrol consists of {num1} veteran(s) and {num2} guard(s). They hail ' \
                                    'from Summit Hall and offer a hearty  "Well Met!" to the characters'
                    # Homestead
                    elif encounter_roll == 7:
                        d6 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        if (d6 == 1) or (d6 == 2) or (d6 == 3):
                            type_ppl = 'Tethyrian human'
                        elif d6 == 4:
                            type_ppl = 'Illuskan human'
                        else:
                            type_ppl = 'halfling'
                        num1 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 6)[0] - 1
                        encounter = f'The homestead consists of {num1} {type_ppl}(s) and their {num2} ' \
                                    'children. Residents might provide friendly adventurers with food and shelter.'
                    # Gargoyles
                    elif encounter_roll == 8:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'{num} gargoyles'
                    # Air cult skyriders
                    elif encounter_roll == 9:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = f'One Feathergale knight leads {num} skyweavers and they are riding giant vultures.'
                    # Water cult raiders
                    elif encounter_roll == 10:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        encounter = f'A group of raiders from the water cult includes {num} Crushing Wave reavers, ' \
                                    'a Crushing Wave priest, and a one-eyed shiver. The leader is a Dark Tide Knight ' \
                                    'mounted on a giant crocodile.'
                    # Bugbears
                    elif encounter_roll == 11:
                        num = dice_roller.multiple_rolls_w_mods(1, 6)[0] + 2
                        encounter = f'{num} bugbears'
                    # Fire cult war band
                    elif encounter_roll == 12:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'A war band of the fire cult consists of {num1} Eternal Flame guardians, an ' \
                                    f'Eternal Flame priest, and {num2} hell hounds.'
                    # Earth cult marauders
                    elif encounter_roll == 13:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4) - 1
                        if num2 == 0:
                            encounter = f'A band of marauders for the earth cult consists of {num1} Black Earth ' \
                                        'guards and a Black Earth priest.'
                        else:
                            encounter = f'A band of marauders for the earth cult consists of {num1} Black Earth ' \
                                        f'guards, a Black Earth priest, and {num2} ogre(s).'
                    # Ogres
                    elif encounter_roll == 14:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = f'{num} ogres'
                    # Caravan
                    elif encounter_roll == 15:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 2
                        num2 = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = 'A caravan consists of a merchant and his or her entourage heading for the ' \
                                    f'nearest settlement. The group consists of {num1} guard(s), ' \
                                    f'{num2} commoner(s), and the caravan lead (a spy).'
                    # Mephits
                    elif encounter_roll == 16:
                        d6 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        if d6 == 1:
                            mephit = 'dust'
                        elif d6 == 2:
                            mephit = 'ice'
                        elif d6 == 3:
                            mephit = 'magma'
                        elif d6 == 4:
                            mephit = 'mud'
                        elif d6 == 5:
                            mephit = 'smoke'
                        else:
                            mephit = 'steam'
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = f'{num} {mephit} mephits'
                    # Dwarf miners
                    elif encounter_roll == 17:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'A band of dwarf miners consists of {num} shield dwarf scouts and a ' \
                                    'pugnacious leader (a shield dwarf thug).'
                    # Elementals
                    elif encounter_roll == 18:
                        d4 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        if d4 == 1:
                            elemental = 'air'
                        elif d4 == 2:
                            elemental = 'earth'
                        elif d4 == 3:
                            elemental = 'fire'
                        else:
                            elemental = 'water'
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} {elemental} elementals'
                    # Bulette
                    elif encounter_roll == 19:
                        encounter = 'One bulette'
                    # Hill giants
                    else:
                        num = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'{num} hill giant(s)'

                # High-Level Party at Night
                else:
                    # Jackalweres
                    if encounter_roll == 2:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        encounter = f'{num} jackalweres'
                    # Manticores
                    elif encounter_roll == 3:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 3))
                        encounter = f'{num} manticore(s)'
                    # Trolls
                    elif encounter_roll == 4:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 3)) + 1
                        encounter = f'{num} trolls'
                    # Elk Tribe Hunters
                    elif encounter_roll == 5:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        encounter = f'This group includes a berserker and {num} tribal warriors. They are hostile.'
                    # Will-o-wisps
                    elif encounter_roll == 6:
                        num = dice_roller.multiple_rolls_w_mods(1, 8)[0]
                        encounter = f'{num} will-o-wisp(s)'
                    # Ghasts and Ghouls
                    elif encounter_roll == 7:
                        num1 = sum(dice_roller.multiple_rolls_w_mods(1, 2))
                        num2 = sum(dice_roller.multiple_rolls_w_mods(1, 4)) + 2
                        encounter = f'{num1} ghast(s) and {num2} ghouls'
                    # Gargoyles
                    elif encounter_roll == 8:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 4)) + 1
                        encounter = f'{num} gargoyles'
                    # Air cult skyriders
                    elif encounter_roll == 9:
                        num = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        encounter = f'One Feathergale knight leads {num} skyweavers and they are riding giant vultures.'
                    # Water cult raiders
                    elif encounter_roll == 10:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 6))
                        encounter = f'A group of raiders from the water cult includes {num} Crushing Wave reavers, ' \
                                    'a Crushing Wave priest, and a one-eyed shiver. The leader is a Dark Tide Knight ' \
                                    'mounted on a giant crocodile.'
                    # Bugbears
                    elif encounter_roll == 11:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 6)) + 2
                        encounter = f'{num} bugbears'
                    # Fire cult war band
                    elif encounter_roll == 12:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        num2 = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'A war band of the fire cult consists of {num1} Eternal Flame guardians, an ' \
                                    f'Eternal Flame priest, and {num2} hell hounds.'
                    # Earth cult marauders
                    elif encounter_roll == 13:
                        num1 = dice_roller.multiple_rolls_w_mods(1, 4)[0] + 1
                        num2 = dice_roller.multiple_rolls_w_mods(1, 4) - 1
                        if num2 == 0:
                            encounter = f'A band of marauders for the earth cult consists of {num1} Black Earth ' \
                                        'guards and a Black Earth priest.'
                        else:
                            encounter = f'A band of marauders for the earth cult consists of {num1} Black Earth ' \
                                        f'guards, a Black Earth priest, and {num2} ogre(s).'
                    # Ogres
                    elif encounter_roll == 14:
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = f'{num} ogres'
                    # Wights
                    elif encounter_roll == 15:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 4)) + 1
                        encounter = f'{num} wights'
                    # Mephits
                    elif encounter_roll == 16:
                        d6 = dice_roller.multiple_rolls_w_mods(1, 6)[0]
                        if d6 == 1:
                            mephit = 'dust'
                        elif d6 == 2:
                            mephit = 'ice'
                        elif d6 == 3:
                            mephit = 'magma'
                        elif d6 == 4:
                            mephit = 'mud'
                        elif d6 == 5:
                            mephit = 'smoke'
                        else:
                            mephit = 'steam'
                        num = sum(dice_roller.multiple_rolls_w_mods(2, 4))
                        encounter = f'{num} {mephit} mephits'
                    # Vampire spawn
                    elif encounter_roll == 17:
                        num = sum(dice_roller.multiple_rolls_w_mods(1, 3))
                        encounter = f'{num} vampire spawn'
                    # Elementals
                    elif encounter_roll == 18:
                        d4 = dice_roller.multiple_rolls_w_mods(1, 4)[0]
                        if d4 == 1:
                            elemental = 'air'
                        elif d4 == 2:
                            elemental = 'earth'
                        elif d4 == 3:
                            elemental = 'fire'
                        else:
                            elemental = 'water'
                        num = dice_roller.multiple_rolls_w_mods(1, 3)[0]
                        encounter = f'{num} {elemental} elementals'
                    # Bulette
                    elif encounter_roll == 19:
                        encounter = 'One bulette'
                    # Hill giants
                    else:
                        num = dice_roller.multiple_rolls_w_mods(1, 2)[0]
                        encounter = f'{num} hill giant(s)'

    encounter = str(encounter)
    return encounter


# Sacred Stone Monastery Mines Encounters
def sacred_stone_monastery_mines(day_or_night: str) -> str:
    """
    This function simulates a mine encounter at the Sacred Stone Monastery.

    Args:
        day_or_night: A string indicating whether the encounter occurs during the day ('day') or at night ('night').

    Returns:
        A string description of the mine encounter, or 'No encounter.' if no encounter occurred.
    """
    d20 = dice_roller.single_roll_w_mod(20, 1, 0)
    if d20 >= 18:
        d6 = dice_roller.single_roll_w_mod(6, 1, 0)
        d8 = dice_roller.single_roll_w_mod(8, 1, 0)
        if day_or_night == 'day':
            roll = d6
        else:  # 'night'
            roll = d6 + d8
        if roll == 1:
            d4 = dice_roller.single_roll_w_mod(4, 1, 0)
            mine_encounter = f'{d4} commoners (see area M19)'
        elif roll == 2:
            d4 = dice_roller.single_roll_w_mod(4, 1, 0)
            mine_encounter = f'{d4} commoners and 1 orog (see area M18)'
        elif roll == 3:
            d3 = dice_roller.single_roll_w_mod(3, 1, 0)
            mine_encounter = f'{d3} Sacred Stone monks (see area M7)'
        elif roll == 4:
            d3 = dice_roller.single_roll_w_mod(3, 1, 0)
            mine_encounter = f'{d3} violet fungi'
        elif roll == 5:
            mine_encounter = '1 grick'
        elif roll == 6:
            d2 = dice_roller.single_roll_w_mod(2, 1, 0)
            mine_encounter = f'Jurth and {d2} orog(s) (see area M18)'
        else:
            num = dice_roller.single_roll_w_mod(3, 1, 1)
            mine_encounter = f'{num} duergar (see area M5)'
    else:
        mine_encounter = 'No encounter.'
    return mine_encounter


# Dark Stream Encounters
def dark_stream() -> str:
    """
    Generates a random encounter in the Dark Stream.

    Returns:
        A string description of the encounter, or 'None' if no encounter occurred.
    """
    d20 = dice_roller.single_roll_w_mod(1, 20, 0)
    if d20 == 1:
        roll = dice_roller.single_roll_w_mod(1, 6, 0)
        dark_stream_encounter = f'{roll} darkmantles'
    elif d20 == 2:
        roll = dice_roller.single_roll_w_mod(1, 4, 1)
        dark_stream_encounter = f'{roll} piercers'
    elif d20 == 3:
        roll = dice_roller.single_roll_w_mod(1, 4, 4)
        dark_stream_encounter = f'{roll} troglodytes'
    elif d20 == 4:
        roll = dice_roller.single_roll_w_mod(1, 4, 1)
        dark_stream_encounter = f'{roll} shadows'
    elif d20 == 5:
        dark_stream_encounter = '1 water weird'
    elif d20 == 2:
        dark_stream_encounter = '1 chull'
    else:
        dark_stream_encounter = 'None'
    return dark_stream_encounter


# The Weeping Colossus Encounters
def weeping_colossus() -> str:
    """
        Rolls a d20 and returns a string describing the encounter in the Weeping Colossus, depending on the roll result:
        1-2: 1 fire elemental
        3: 1-8 magmin
        4: 1-3 salamanders
        5: Crust Break: A random character falls into a hole or a pool of lava
        6: Flame Gout: A bubble of gas ignites, dealing fire damage and igniting flammable items
        7: Lava Rain: Lava droplets fall on a 20-foot diameter area, dealing fire damage and igniting flammable items
        8: Smoke Cloud: A cloud of smoke fills a 40-foot diameter area, causing it to become lightly obscured
        9: Sulfur Cloud: A cloud of sulfur creates a poisonous cloud in a 30-foot diameter area
        10: Lava surge: A lava surge fills a 30-foot diameter area, dealing fire damage and igniting flammable items
        11: Magma column: A magma column erupts, dealing fire damage and igniting flammable items
        12: Ceiling collapse: A portion of the ceiling collapses, dealing bludgeoning damage
        13-20: No encounter

        Returns:
            str: A string describing the encounter in the Weeping Colossus.
        """
    d20 = dice_roller.single_roll_w_mod(1, 20, 0)
    # Fire elemental
    if d20 < 3:
        encounter = '1 fire elemental'
    # Magmin
    elif d20 == 3:
        roll = dice_roller.single_roll_w_mod(1, 8, 0)
        encounter = f'{roll} magmin'
    # Salamander
    elif d20 == 4:
        roll = dice_roller.single_roll_w_mod(1, 3, 0)
        encounter = f'{roll} salamanders'
    # Crust break
    elif d20 == 5:
        lava_yes_or_no = dice_roller.single_roll_w_mod(1, 4, 0)
        if lava_yes_or_no == 1:
            encounter = 'Crust Break:\nA random character steps on a particularly thin spot on the floor. The \n' \
                        'character must succeed at a DC 10 Dexterity saving throw or fall into a pool of lava'
        else:
            hole_depth = dice_roller.single_roll_w_mod(1, 4, 0)*10
            encounter = f'Crust Break:\nA random character steps on a particularly thin spot on the floor. The \n' \
                        f'character must succeed at a DC 10 Dexterity saving throw or fall into a {hole_depth} feet ' \
                        f'deep hole.'
    # Flame gout
    elif d20 == 6:
        character = ['Tar Tar', 'Daang', 'Stump Chump', 'Z'][dice_roller.single_roll_w_mod(1, 4, 0)-1]
        direction1 = ['Up', 'Down', 'Left', 'Right', 'Forward', 'Backward'][dice_roller.single_roll_w_mod(1, 6, 0)-1]
        direction2 = ['Up', 'Down', 'Left', 'Right', 'Forward', 'Backward'][dice_roller.single_roll_w_mod(1, 6, 0)-1]
        direction = direction1 + '-' + direction2
        fire_dmg = dice_roller.single_roll_w_mod(3, 6, 0)
        if direction in ['Up-Down', 'Down-Up', 'Forward-Backward', 'Backward-Forward', 'Left-Right', 'Right-Left']:
            encounter = f'Flame Gout:\nA bubble of gas spontaneously ignites creating a burst of flame. A 5-foot \n' \
                        f'radius sphere engulfs {character} dealing {fire_dmg} fire damage and igniting any \n' \
                        f'flammable items within that aren\'t being held or worn.'
        else:
            encounter = f'Flame Gout:\nA bubble of gas spontaneously ignites creating a burst of flame. A 15-foot \n' \
                        f'cone starts near {character} and shoots in a {direction} direction. Anyone caught in it\n' \
                        f'takes {fire_dmg} fire damage. Any flammable items that aren\'t being worn or held ignite.'
    # Lava rain
    elif d20 == 7:
        dmg = dice_roller.single_roll_w_mod(2, 8, 0)
        end_point = dice_roller.single_roll_w_mod(2, 4, 0)
        encounter = f'Lava Rain:\nAt the start of a round, lava droplets form on the ceiling and fall in a \n' \
                    f'20-foot diameter area, dealing {dmg} fire damage to any creature that enters the area for \n' \
                    f'the first time on a turn or starts its turn there. The drops ignite exposed flammable objects.' \
                    f'\nThe rain ends {end_point} rounds later.'
    # Smoke cloud
    elif d20 == 8:
        roll = dice_roller.single_roll_w_mod(1, 4, 0)
        encounter = f'Smoke Cloud:\nA cloud of smoke kicks up filling an area 40 feet in diameter and causing that \n' \
                    f'area to become lightly obscured. The smoke dissipates in {roll} minutes unless a \n' \
                    f'wind disperses it.'
    # Sulfur cloud
    elif d20 == 9:
        encounter = 'Sulfur Cloud:\nA discharge of noxious fumes erupt from a nearby vent. There\'s a \n' \
                    '20-foot-radius sphere of yellow, nauseating gas. The cloud spreads around corners, and \n' \
                    'the area is heavily obscured. The cloud lingers in the air until dispered.\n' \
                    '\n' \
                    'Each creature that is completely within the cloud at the start of its turn must make \n' \
                    'a Constitution saving throw against poison. On a failed save, \n' \
                    'the creature spends its action that turn retching and reeling. Creatures \n' \
                    'that don\'t need to breathe or are immune to poison automatically succeed on \n' \
                    'this saving throw.\n' \
                    '\n' \
                    'A moderate wind (at least 10 miles per hour) disperses the cloud after 4 rounds. A strong \n' \
                    'wind (at least 20 miles per hour) disperses it after 1 round.'
    # None
    else:
        encounter = 'No encounter'
    return encounter


# Temple of Howling Hatred Encounters
def temple_of_howling_hatred() -> str:
    """
        Rolls a 1d20 and returns a string describing the encounter in the Temple of Howling Hatred, depending on the
        roll result:
        1-2: 2-8 Howling Hatred initiates
        3: 1-4 hurricanes
        4: 1-2 skyweavers
        5: 1-2 Howling Hatred priests
        6: 1 kenku
        7-20: No encounter

        Returns:
            str: A string describing the encounter in the Temple of Howling Hatred.
        """
    d20 = dice_roller.single_roll_w_mod(1, 20, 0)
    # Howling hatred initiates
    if d20 < 3:
        roll = dice_roller.single_roll_w_mod(2, 4, 0)
        encounter = f'{roll} Howling Hatred initiates'
    # Hurricanes
    elif d20 == 3:
        roll = dice_roller.single_roll_w_mod(1, 4, 0)
        encounter = f'{roll} hurricanes'
    # Skyweavers
    elif d20 == 4:
        roll = dice_roller.single_roll_w_mod(1, 2, 0)
        encounter = f'{roll} skyweavers'
    # Howling Hatred Priests
    elif d20 == 5:
        roll = dice_roller.single_roll_w_mod(1, 2, 0)
        encounter = f'{roll} Howling Hatred priests'
    # Kenku
    elif d20 == 6:
        encounter = '1 kenku'
    # None
    else:
        encounter = 'No encounter'
    return encounter


# The Canals
def canals() -> str:
    """
        Rolls a 1d20 and returns a string describing the encounter in the canals, depending on the roll result:
        1: 1-4 ghouls
        2-3: 1-4 lizardfolk
        4: 1-2 trolls
        5: 1 giant octopus
        6: 1 dragon turtle
        7-20: No encounter

        Returns:
            str: A string describing the encounter in the canals.
        """
    d20 = dice_roller.single_roll_w_mod(1, 20, 0)
    # Ghouls
    if d20 == 1:
        roll = dice_roller.single_roll_w_mod(1, 4, 1)
        encounter = f'{roll} ghouls'
    # Lizardfolk
    elif d20 < 4:
        roll = dice_roller.single_roll_w_mod(1, 4, 4)
        encounter = f'{roll} lizardfolk'
    # Trolls
    elif d20 == 4:
        roll = dice_roller.single_roll_w_mod(1, 2)
        if roll == 1:
            encounter = '1 troll'
        else:
            encounter = '2 trolls'
    # Giant Octopus
    elif d20 == 5:
        encounter = '1 giant octopus'
    # Dragon Turtle
    elif d20 == 6:
        encounter = '1 dragon turtle'
    # None
    else:
        encounter = 'No encounter'
    return encounter


# Monster creator based on environment
def environment(env: str = 'All') -> None:
    """Generate monster given environment

    Environment Choices:
    Arctic, Coastal, Desert, Forest, Grassland, Hill, Jungle, Mountain, Swamp, Underdark, Underwater, Urban

    Args:
        env (str): Optionally declare the environment that you want to create a monster for.
    """
    monster_df = pd.read_excel(io='monsters.xlsx', sheet_name=None)

    encounter_index = dice_roller.single_roll_w_mod(1, len(monster_df[env]), 0)
    encounter = monster_df[env].loc[encounter_index, :]
    print()
    print(f'encounter:\n{encounter}')
    print('-'*45)
    return None
