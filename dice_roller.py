import random


def single_roll_w_mod(number_of_dice: int, type_of_dice: int, mod: int = 0, show_roll: bool = False) -> int:
    """Simulates rolling one or more dice then adding/subtracting a modifier if there is one.

    Args:
        type_of_dice (int): The type of dice that are being rolled.
        number_of_dice (int, optional): The number of dice being rolled. Default is 1.
        mod (int, optional): Roll modifier to add to sum of rolls. Default is 0.

    Returns:
        total_w_mod (int): Roll total with the modifier added
    """
    list_of_rolls = [random.randint(1, type_of_dice) for i in range(number_of_dice)]
    total_wo_mod = sum(list_of_rolls)
    total_w_mod = total_wo_mod + mod
    total_w_mod = int(total_w_mod)

    if show_roll is True:
        print(f'Roll: {total_w_mod}')
        print()

    return total_w_mod


def multiple_rolls_w_mods(
        number_of_dice_per_roll: int,
        type_of_dice: int,
        modifier: int,
        number_of_rolls: int,
        show_rolls: bool = True) -> None:
    """Simulates rolling multiple dice with modifiers added to each roll individually.

    Args:
        number_of_dice_per_roll (int): Number of dice being rolled per roll.
        type_of_dice (int): Type of dice being rolled per roll.
        modifier (int): The modifier being added per roll.
        number_of_rolls (int): The number of rolls being performed.
        show_results (bool): Indicate if you want the results to be show. Default True.

    Returns:
        list_of_rolls (list(int)): List of rolls with the specified modifier added to each roll.
    """
    list_of_rolls = [
        single_roll_w_mod(number_of_dice_per_roll, type_of_dice, mod=modifier) for i in range(number_of_rolls)]

    if show_rolls is True:
        print(f'List of rolls: {list_of_rolls}')
        print('-'*20)
        print(f'SUM of {len(list_of_rolls)} rolls: {sum(list_of_rolls)}')
        print(f'AVG of {len(list_of_rolls)} rolls: {sum(list_of_rolls)/len(list_of_rolls)}')
        print()

    return list_of_rolls


def main():
    func = input("Roll for a single value or multiple values from dice rolls?\nInput 'Single' or 'Multiple': ")
    if func.lower() == 'single':
        print("\nRunning 'single_roll_w_mod' function...")
        type_dice = int(input("\nType of dice? "))
        num_dice = int(input("How many dice? "))
        modifier = int(input("Modifier? "))
        print()
        single_roll_w_mod(number_of_dice=num_dice, type_of_dice=type_dice, mod=modifier, show_roll=True)

    elif func.lower() == 'multiple':
        print("\nRunning 'multiple_rolls_w_mods' function...")
        number_of_rolls = int(input("\nNumber of rolls? "))
        type_dice = int(input("Type of dice? "))
        num_dice = int(input("How many dice? "))
        modifier = int(input("Modifier? "))
        print()
        multiple_rolls_w_mods(
            number_of_dice_per_roll=num_dice,
            type_of_dice=type_dice,
            modifier=modifier,
            number_of_rolls=number_of_rolls,
            show_rolls=True)
    return None


if __name__ == "__main__":
    main()
