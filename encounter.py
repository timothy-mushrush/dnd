import encounter_util as eu


def main():
    encounter = eu.canals()
    print()
    print('encounter:')
    print('----------')
    print(encounter)
    print()
    print('-'*45)
    return None


if __name__ == "__main__":
    main()
