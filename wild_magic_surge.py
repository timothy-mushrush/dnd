import dice_roller


def wild_magic_surge_table_effect(roll: int) -> str:
    """Generates effect given a roll on the Wild Magic Surge Table

    Args:
        roll (int): d100 roll

    Returns:
        effect (str): The effect resulting from Wild Magic Surge
    """
    if (roll == 1) | (roll == 2):
        effect = "Roll on this table at the start of each of your turns for the next minute, rerolling this result on subsequent rolls."
    elif (roll == 3) | (roll == 4):
        effect = "You regain all expended sorcery points."
    elif (roll == 5) | (roll == 6):
        effect = "You cast blindness/deafness on yourself with both effects, rolling your saving throw separately for each."
    elif (roll == 7) | (roll == 8):
        effect = "You cast fireball as a 3rd-level spell centered on yourself. Damage caused by this feature cannot reduce a creature to 0 hit points."
    elif (roll == 9) | (roll == 10):
        effect = "You cast magic missile as an Xth-level spell targeting a creature of the DMs choice, where X equals your Charisma modifier (minimum of 1)."
    elif (roll == 11) | (roll == 12):
        effect = "Your height changes by 1d6 inches. If the roll is odd, you shrink. If the roll is even, you grow. If this effect is already in place when you roll this, you return to your original height. A remove curse spell can end this effect."
    elif (roll == 13) | (roll == 14):
        effect = "You cast hypnotic pattern centered on yourself."
    elif (roll == 15) | (roll == 16):
        effect = "You cast spiritual weapon in the nearest unoccupied space, and it immediately attacks you if possible. The weapon moves on the shortest path toward you before each of your turns and attacks you if possible. It disappears if you fall unconscious."
    elif (roll == 17) | (roll == 18):
        effect = "You grow a long beard made of feathers that remains until you sneeze, at which point the feathers explode out from your face."
    elif (roll == 19) | (roll == 20):
        effect = "You cast grease centered on yourself. The grease puddle moves with you."
    elif (roll == 21) | (roll == 22):
        effect = "Creatures have disadvantage on saving throws against the next spell you cast before your next long rest that involves a saving throw."
    elif (roll == 23) | (roll == 24):
        effect = "Your eyes and hair turn a random color determined by a 1d6. 1- red, 2- orange, 3- yellow, 4- green, 5-blue, 6-purple. If this effect is already in place when you roll this, they return to your original color. A remove curse spell ends this effect."
    elif (roll == 25) | (roll == 26):
        effect = "You gain one of the following incorporeal traits based on your alignment until your next long rest. Good- a halo floats above your head. Neutral- a flower blooms on your head. Evil- you sprout large devil horns."
    elif (roll == 27) | (roll == 28):
        effect = "For the next minute, all your spells with a casting time of 1 action have a casting time of 1 bonus action."
    elif (roll == 29) | (roll == 30):
        effect = "Up to 4 creatures you choose within 30 feet of you take 4d10 lighting damage."
    elif (roll == 31) | (roll == 32):
        effect = "You cast blink."
    elif (roll == 33) | (roll == 34):
        effect = "Maximize the damage of the next damaging spell you cast before you finish a long rest."
    elif (roll == 35) | (roll == 36):
        effect = "Roll a d6. Your age changes by a number of years equal to the roll. If the roll is odd, you get younger (minimum 1 year old). If the roll is even, you get older. If this effect is already in place when you roll this, you return to your original age. A remove curse spell ends this effect."
    elif (roll == 37) | (roll == 38):
        effect = "You cast inflict wounds on the nearest creature within 30 feet of you at the end of your next turn. If there aren’t any creatures nearby, you cast it on yourself."
    elif (roll == 39) | (roll == 40):
        effect = "You cast cure wounds as a 1d4th level spell on yourself."
    elif (roll == 41) | (roll == 42):
        effect = "You turn into a potted plant until the start of your next turn. While a plant, you are incapacitated and have vulnerability to all damage. If you drop to 0 hit points, your pot breaks, and your form reverts."
    elif (roll == 43) | (roll == 44):
        effect = "You cast thunder step as a 3rd level spell."
    elif (roll == 45) | (roll == 46):
        effect = "You cast levitate on yourself."
    elif (roll == 47) | (roll == 48):
        effect = "A unicorn controlled by the DM appears in a space within 5 feet of you, then disappears 1 minute later."
    elif (roll == 49) | (roll == 50):
        effect = "You can't speak for the next minute. Whenever you try, rainbow colored bubbles float out of your mouth."
    elif (roll == 51) | (roll == 52):
        effect = "A spectral shield hovers near you for the next minute, granting you a +2 bonus to AC and immunity to magic missile."
    elif (roll == 53) | (roll == 54):
        effect = "You are immune to being intoxicated by alcohol for the next 5d6 days."
    elif (roll == 55) | (roll == 56):
        effect = "You cast astral projection on only yourself."
    elif (roll == 57) | (roll == 58):
        effect = "For the next minute, any flammable object you touch that isn't being worn or carried by another creature bursts into flame."
    elif (roll == 59) | (roll == 60):
        effect = "You regain an expended spell slot of 5th level of lower (player choice)."
    elif (roll == 61) | (roll == 62):
        effect = "For the next minute, your voice booms three times as loudly when you speak."
    elif (roll == 63) | (roll == 64):
        effect = "You cast fog cloud centered on yourself. The fog cloud moves with you."
    elif (roll == 65) | (roll == 66):
        effect = "You cast call lightning, but it lasts 3 rounds instead of the usual 10 minutes."
    elif (roll == 67) | (roll == 68):
        effect = "You are frightened by the nearest creature until the end of your next turn."
    elif (roll == 69) | (roll == 70):
        effect = "You and each creature within 30 feet of you becomes invisible for the next minute."
    elif (roll == 71) | (roll == 72):
        effect = "You gain resistance to all damage for the next minute."
    elif (roll == 73) | (roll == 74):
        effect = "A creature of your choice within 60 feet of you becomes poisoned."
    elif (roll == 75) | (roll == 76):
        effect = "You glow with golden bright light in a 30-foot radius for the next minute, and your hair becomes gold and stands straight up. Any creature that ends its turn within 5 feet of you is blinded by your radiant glory until the end of its next turn."
    elif (roll == 77) | (roll == 78):
        effect = "You cast polymorph on yourself. If you fail the saving throw, you turn into a sheep for the spell's duration."
    elif (roll == 79) | (roll == 80):
        effect = "Illusory butterflies and flower petals flutter in the air within 10 feet of you until your next long rest."
    elif (roll == 81) | (roll == 82):
        effect = "You can take one additional action immediately."
    elif (roll == 83) | (roll == 84):
        effect = "Each creature within 30 feet of you takes 1d10 necrotic damage. You regain hit points equal to the sum of the necrotic damage dealt."
    elif (roll == 85) | (roll == 86):
        effect = "You cast mirror image."
    elif (roll == 87) | (roll == 88):
        effect = "You cast fly on yourself and every creature in a 30 foot radius with a duration of 1 minute instead of the usual 10."
    elif (roll == 89) | (roll == 90):
        effect = "You become invisible for the next minute. During that time, other creatures can't hear you. The invisibility ends if you attack or cast a spell."
    elif (roll == 91) | (roll == 92):
        effect = "If you die within the next minute, you immediately come back to life with full hit points, rolling on this table when you do so (even if you already surged that turn)."
    elif (roll == 93) | (roll == 94):
        effect = "You cast enlarge on yourself."
    elif (roll == 95) | (roll == 96):
        effect = "You and all creatures within 30 feet of you gain vulnerability to all damage for the next round."
    elif (roll == 97) | (roll == 98):
        effect = "You are surrounded by faint (but lively) music until your next long rest."
    elif (roll == 99) | (roll == 100):
        effect = "Choose any surge you want (besides this one)."
    
    return effect


def main():
    print()
    effect = wild_magic_surge_table_effect(dice_roller.single_roll_w_mod(1, 100, 0, show_roll=False))
    print(f'Wild Magic Surge Effect:\n\n{effect}')
    print('-'*60)
    return None


if __name__ == "__main__":
    main()
